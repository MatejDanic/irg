from pyglet.gl import *

klik = 1

xs, ys, xe, ye = 0, 0, 0, 0
xls, yls, xle, yle = 0, 0, 0, 0

window = pyglet.window.Window(width=400, height=400, resizable='false')


@window.event
def on_mouse_press(x, y, button, modifiers):
    global klik, xs, ys, xe, ye, xls, yls, xle, yle
    klik += 1

    if klik % 2 == 0:
        xs = x
        ys = y
        xls, yls = xs, ys

    else:
        xe = x
        ye = y
        xle, yle = xe, ye

        if xs <= xe:
            if ys <= ye:
                bresenham2(xs, ys, xe, ye)
            else:
                bresenham3(xs, ys, xe, ye)
        else:
            if ys >= ye:
                bresenham2(xe, ye, xs, ys)
            else:
                bresenham3(xe, ye, xs, ys)

        pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ('v2i', (xls, yls + 20, xle, yle + 20)))


def bresenham2(xs, ys, xe, ye):
    if ye-ys <= xe-xs:
        a = 2*(ye-ys)
        yc = ys
        yf = -(xe-xs)
        korekcija = -2*(xe-xs)
        for x in range(xs, xe):
            pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (x, yc)))
            yf = yf + a
            if yf >= 0:
                yf = yf + korekcija
                yc = yc + 1
    else:
        xs, ys, xe, ye = ys, xs, ye, xe
        a = 2*(ye-ys)
        yc = ys
        yf = -(xe-xs)
        korekcija = -2*(xe-xs)
        for x in range(xs, xe):
            pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (yc, x)))
            yf = yf + a
            if yf >= 0:
                yf = yf + korekcija
                yc = yc + 1


def bresenham3(xs, ys, xe, ye):
    if -(ye-ys) <= xe-xs:
        a = 2*(ye-ys)
        yc = ys
        yf = xe-xs
        korekcija = 2*(xe-xs)
        for x in range(xs, xe):
            pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (x, yc)))
            yf = yf + a
            if yf <= 0:
                yf = yf + korekcija
                yc = yc - 1
    else:
        xe, ye, xs, ys = ys, xs, ye, xe
        a = 2*(ye-ys)
        yc = ys
        yf = xe-xs
        korekcija = 2*(xe-xs)
        for x in range(xs, xe):
            pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (yc, x)))
            yf = yf + a
            if yf <= 0:
                yf = yf + korekcija
                yc = yc - 1


@window.event
def on_draw():
    glClearColor(0, 0, 0, 0)


pyglet.app.run()
