import numpy

vrh_A = [0, 0, 0]
vrh_B = [0, 0, 0]
vrh_C = [0, 0, 0]
tocka_T = [0, 0, 0]

for i in range (4):
    for j in range(3):
        if i == 0:
            vrh_A[j] = int(input())
        elif i == 1:
            vrh_B[j] = int(input())
        elif i == 2:
            vrh_C[j] = int(input())
        else:
            tocka_T[j] = int(input())

jed = numpy.matrix.__invert__(numpy.matrix([vrh_A, vrh_B, vrh_C]))
baricentricne_koordinate = numpy.linalg.solve(jed, tocka_T)

print('[t1, t2, t3] = [{}]'.format(baricentricne_koordinate))
