import numpy

jed_1 = [0, 0, 0]
jed_2 = [0, 0, 0]
jed_3 = [0, 0, 0]
desna_strana = [0, 0, 0]

for i in range(3):
    for j in range(4):
        if j == 3:
            desna_strana[i] = int(input())
        elif i == 0:
            jed_1[j] = int(input())
        elif i == 1:
            jed_2[j] = int(input())
        else:
            jed_3[j] = int(input())

jed = numpy.array([jed_1, jed_2, jed_3])
desna_strana = numpy.array(desna_strana)

try:
    rjes = numpy.linalg.solve(jed, desna_strana)
except numpy.linalg.LinAlgError:
    print('error')

print('{}x + {}y + {}z = '.format(jed_1[0], jed_1[1], jed_1[2]), desna_strana[0])
print('{}x + {}y + {}z = '.format(jed_2[0], jed_2[1], jed_2[2]), desna_strana[1])
print('{}x + {}y + {}z = '.format(jed_3[0], jed_3[1], jed_3[2]), desna_strana[2])
print('[x, y, z] = [{}]'.format(rjes))
