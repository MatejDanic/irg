import numpy

v1_1 = [2, 3, -4]
v1_2 = [-1, 4, -1]
v1 = [0, 0, 0]

for i in range(3):
    v1[i] = v1_1[i] + v1_2[i]
print('v1 = ({}i + {}j + {}k) + ({}i + {}j + {}k) = {}i + {}j + {}k'.format(v1_1[0], v1_1[1], v1_1[2], v1_2[0], v1_2[1], v1_2[2], v1[0], v1[1], v1[2]))
print()

s1 = [-1, 4, -1]
s = 0
for i in range(3):
    s += v1[i] * s1[i]
print('s = v1 * (-i + 4j - k) = ', s)
print()

v2_1 = [2, 2, 4]
v2_1 = numpy.array(v2_1)
v1 = numpy.array(v1)
v2 = numpy.cross(v1, v2_1)
print('v2 = ({}i + {}j + {}k) x (2i + 2j + 4k) = {}i + {}j + {}k'.format(v1[0], v1[1], v1[2], v2[0], v2[1], v2[2]))
print()

v3 = numpy.linalg.norm(v2)
print('v3 = |v2| = ', v3)
print()

v4 = -v2
print('v4 = -v2 = ', v4)
print()

M1_1 = numpy.matrix([[1, 2, 3], [2, 1, 3], [4, 5, 1]])
M1_2 = numpy.matrix([[-1, 2, -3], [5, -2, 7], [-4, -1, 3]])
M1 = M1_1 + M1_2
print('M1 = {} + {} = '.format(M1_1, M1_2), M1)
print()

M2 = M1_1 * numpy.matrix.transpose(M1_2)
print('M2 = {} * {}^T = '.format(M1_1, M1_2), M2)
print()

M3 = M1_1 * numpy.linalg.inv(M1_2)
print('M3 = {} * {}^-1 = '.format(M1_1, M1_2), M3)
print()
