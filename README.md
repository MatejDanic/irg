# IRG

Laboratorijske vježbe iz predmeta Interaktivna računalna grafika.

# Dokumentacija laboratorijskih vježbi

## VJEŽBA 1

### Opis programa

Vježba se sastoji od tri programa za svaki zadatak iz vježbe. Prvi program računa jednostavan zbroj
vektora, skalarni i vektorski umnožak te normiranje i pretvaranje suprotnog smjera vektora. Nakon
toga program radi operacije zbrajanja, transponiranja i množenja matrica. Drugi program dopušta
unos podataka o sustavu tri jednadžbe s tri nepoznanice koji se nakon toga rješava te ispisuje
rezultate nepoznanica. Posljednji program računa baricentrične koordinate trokuta čiji su vrhovi na
početku uneseni.

### Korištene strukture podataka

Vektori iz prvog zadatka, spremljeni su u obliku polja s vrijednostima osnovnih i, j, i k vektora. Za
zbrajanje vektora koristi se obična petlja koja zbraja pojedine i, j i k vrijednosti vektora i rezultira
novim vektorom. Skalarni produkt obavlja se običnom operacijom množenja dok se vektorski
produkt i ostale operacije obavljaju korištenjem funkcija biblioteke numpy. Matrice imaju oblik
dvodimenzionalnog polja koje se također pretvara u posebno numpy polje. Drugi i treći zadatak
podatke sustava jednadžbi i matrica također spremaju u obliku polja.

### Upute za korištenje programa

Prvi zadatak ne sadrži nikakav unos podataka te se može normalno pokrenuti i promatrati ispis
rezultata operacija obavljenih nad vektorima i matricama. Drugi zadatak zahtijeva unos 12 podataka
za sustav triju jednadžbi. Ti podaci su redom koeficijenti uz x, y, z nepoznanice i četvrta vrijednost
koja predstavlja konstantu na desnoj strani jednadžbe. Podaci se unose za prvu, drugi i potom treću
jednadžbu nakon čega se ispisuje rezultat nepoznanica. Treći zadatak zahtijeva unos x, y, i z
koordinata triju točaka koje predstavljaju vrhove trokuta i četvrte točke nad kojom se računaju
baricentrične koordinate koje nakon toga ispisuju.

### Komentar rezultata

Rezultati ove vježbe se zbog jednostavnosti programa izvode veoma brzo i nema problema pri
njihovom izvršavanju. Moguće promjene koje bi se mogle primijeniti je omogućen unos podataka
vektora i matrica koje se koriste u prvom zadatku ove vježbe.


## VJEŽBA 2

### Opis programa

Radni zadatak programa predstavlja implementaciju Bresenham-ovog postupka iscrtavanja linije
određenu dvjema točkama. Implementacija algoritma napravljena je za sve moguće kutove
postavljanja početne i krajnje točke. Program prikazuje usporedbu linija iscrtanih implementiranim
algoritmom i postojećom funkcijom iz biblioteke pyglet za iscrtavanje linije.

### Promjene načinjene s obzirom na upute

Promjena koja je načina, a nije navedena u uputama jest popravak Bresenham-ovog postupka nad
kutevima koji su veći od 45 stupnjeva. Naime pseudokod zadan u uputama funkcionira ako se druga
točka zada između 0 i 45 stupnjeva nakon prve točke s obzirom na horizontalnu os. Postupak je
popravljen tako što se nakon unosa točaka ovisno o njihovom međusobnom položaju određuje koji
će se postupak iscrtavanja koristiti. Za kuteve veće od 45, a manje od 90 stupnjeva, postupak treba
provjeriti razliku promjene x i y koordinata između početne i krajnje točke. Ako je razlika po y-u veća
od razlike po x-u, varijable se zamjenjuju i postupak se nastavlja. Ako se početna točka nalazi nakon
druge točke, definiranim metodama algoritma se predaju zamijenjene vrijednosti koordinata točaka.

### Korištene strukture podataka

X i y koordinate početne i krajnje točke spremaju se u zasebne varijable (xs – x start, xe – x end, ys –
y start, ye – y end). Osim tih koordinata, spremaju se i koordinate točaka za drugu liniju koja će se
iscrtati 20 piksela poviše prve linije i time prikazati usporedbu iscrtavanja.

### Upute za korištenje programa

Program nakon pokretanja stvara crni prozor dimenzija 400x400 na kojem se klikom miša zadaju
koordinate prve i druge točke redom. Program dopušta naknadno unošenje točaka te će nakon
svake druge unesene točke (klik miša) iscrtati sve prethodne i najnoviju liniju). Program završava
pritiskom gumba x na novootvorenom prozoru.

### Komentar rezultata

Rezultati druge vježbe se također izvode u jako kratkom vremenu. Razlog tome je što su korištene
operacije veoma jednostavne. Jedan nedostatak ovog programa je što se nakon svakog unosa prve
točke sakrije prikaz dosad iscrtanih linija. Međutim nakon unosa sljedeće točke, iscrtavaju se sve
prethodno zadane linije. Moguće promjene koje bi se mogle primjeniti su dodavanje animacije crte
koja se vrti u krug i time odmah pokazuje da se ispravno iscrtava za svaki kut od 0 do 360 stupnjeva.


## VJEŽBA 3

### Opis programa

Radni zadatak ove vježbe je iscrtati poligon na zaslonu te odrediti da li se zadana točka nalazi unutar
ili izvan tog poligona. Program pamti unošene koordinate točaka do pritiska određenog gumba
nakon kojeg iscrtava poligon. Poligon se iscrtava crtanjem linija, redom od svake točke do njoj
sljedeće i na kraju od zadnje točke do prve točke. Sljedećim unosom točke određuje se njen odnos sa
zadanim poligonom.

### Korištene strukture podataka

Za svrhe izvršavanja ove vježbe, napravljena je nova struktura podatak, Točka. Točka je struktura
podataka koja u sebi sadrži vrijednosti x i y koordinate spremljene u x_coor i y_coor varijable. Osim
toga struktura ima implementiranu metodu ispisa koja se koristila pri izradi programa u svrhe
provjere i ispravljanja pogrešaka te ispis rezultata provjere odnosa s poligonom.

### Upute za korištenje programa

Program kao i kod prethodne vježbe, nakon pokretanja stvara crni prozor dimenzija 400x400. Na
crnom prozoru klikom miša postavlja se nova točka koja predstavlja vrh poligona. Postupak klikanja
se ponavlja za proizvoljni broj vrhova uz uvjet da se vrhovi dodaju u smjeru kazaljke na satu. Razlog
to je što određivanje da li se neka točka nalazi unutar ili izvan tog poligona ovisi o redoslijedu unosa
vrhova. Nakon što su klikom miša uneseni svi željeni vrhovi, pritiskom na tipku enter crta se naš
poligon. Sljedeći korak je da se klikom miša bilo gdje na prozoru odredi točka kojoj želim saznati
odnos s poligonom. Kako bi ovaj postupak radio, potrebno je da je poligon potpuno konveksan (da
nema udubljene dijelove). Nakon određivanja nove točke ispisuje se poruka o odnosu te točke s
poligonom (nalazi li se unutar ili izvan poligona). Sljedećim klikovima miša, određuje se i ispisuje
odnos svake sljedeće točke i prvog poligona. Program završava pritiskom gumba x na
novootvorenom prozoru.

### Komentar rezultata

Izvođenje ovog programa je dosta kratko jer kao i kod prošlih primjera, nema kompliciranih i
složenih operacije koje zahtijevaju veću računarsku „moć“. Nedostatak koji ovaj program ima je
sličan kao i kod prethodne vježbe u tome da se nakon klika miša za određivanje točke (nakon što je
iscrtan poligon), poligon sakrije te je potrebno ili kliknuti ponovno miš za novu točku ili tipku enter
kako bi se ponovno stvorio. Nakon svakog drugog unosa točke poligon se ponovno iscrtava. Moguće
promjene koje bi se mogle primijeniti su implementacija postupka određivanja odnosa točke i
poligona za konkavne poligone.


## VJEŽBA 4

### Opis programa

Program četvrte vježbe iscrtavanje poligona zadanog iz datoteke tipa .obj te određivanje odnosa
zadane točke s tim poligonom. Datoteke u kojima se nalazi opis poligona sadrže vrijednosti x, y, i z
koordinate svakog vrha te popis svih ravnina (poligona) koje čine tijelo. Ravnina uvijek sadrži tri vrha
koja ju određuju, a zapisani su rednim brojem. Iscrtavanje poligona radi se prolaskom kroz sve
ravnine te iscrtavanjem vrijednosti svake točke te ravnine. Pri iscrtavanju, z koordinate su
zanemarene jer se nam je potrebna samo projekcija slike na z os.

### Promjene načinjene s obzirom na upute

Redni brojevi točaka pojedinih ravnina zapisanih u datotekama napisani su tako da počinju od broja

1. Pošto strukture podataka u programskom jeziku Python, točnije liste u koje će se spremait te
vrijednosti počinju od nule, u zapisu će se redni broj svake točke umanjiti za 1.

### Korištene strukture podataka

U ovoj vježbi, dodana je struktura podataka definira identično kao i kod prethodne vježbe. Osim
toga, definirana je nova struktura podataka, Ravnina. Ravnina je struktura podataka koja u sebi
sadrži redni triju točaka kojima je određena. Isto kao i kod točke implementirana je metoda za
reprezentaciju strukture u standardnom ispisu. Zapisi svih točaka i ravnina nalazit će se u listama te
će se do pojedinim podacima pristupati rednim brojem (indeksom) određene liste.

### Upute za korištenje programa

Ovaj program u početku prolazi kroz datoteku te zapisuje podatke o točkama i ravninama, a ne
stvara odmah prozor s iscrtanim objektom. Prije toga je potrebno unijeti vrijednosti x, y, i z
koordinata točke za koju želimo saznati odnos s poligonom. Nakon toga stvara se prozor, a pritiskom
gumba enter iscrtavaju se poligon i zadana točka. Također ispisuje se poruka o tome da li se točka u
trodimenzionalnom prostoru nalazi izvan ili unutar zadanog poligona.

### Komentar rezultata

Ovisno o zadanom objektu, tj. količini njegovih poligona i točaka, sami proces iscrtavanja može
potrajati između jedne i dvije sekunde. Nedostatak ovog programa je nemogućnost promjene
objekta koji želimo iscrtati i za kojeg želimo odrediti odnos sa zadanom točkom. Da se taj objekt
promjeni potrebno je u programskom kodu promjeniti put do željene datoteke objekta. Moguće
promijene koje bi se mogle primijeniti automatsko skaliranje i translacija objekta ovisno o njegovoj
veličini i položaju. Trenutno, svaki objekt se ne može ispravno iscrtati koristeći identične vrijednosti
faktora skaliranja i translacije. Za svaki objekt potrebno je zadati drugačije vrijednosti.


## VJEŽBA 5

### Opis programa

Cilj ove vježbe je transformacija pogleda i izrada perspektivne projekcije za poligone iz prethodne
vježbe. Potrebno je odrediti koordinate očišta i gledišta u trodimenzionalnom prostoru te simulirati
pogled na objekt. Kako bi to ostvarili potrebno je nad svakom točkom poligona obaviti operacije
translacije, rotacije i skaliranja korištenjem za to određenim matricama. Čitanje iz datoteke se
obavlja isto kao i kod prethodne vježbe. Za matrice rotacije korišten je drugi način iz uputa za
laboratorijsku vježbu.

### Korištene strukture podataka

U ovoj vježbi, korištene su strukture podataka polja koja spremaju vrijednosti x, y, i z koordinata
točaka i redni broj točaka u ravninama. Liste su korištene za spremanje pojedinih točki i ravnina
kojima se pristupa isključivo rednim brojem liste. Za matrice transformacije korištena su
dvodimenzionalna polja koja su pretvorena u numpy polja kako bi nad njima mogli obavljati željene
operacije, npr.množenja.

### Upute za korištenje programa

Program u samom početku zahtijeva unos x, y i z koordinata očišta i gledišta. Nakon uspješnog
unosa, otvara se crni prozor na kojem će se klikom gumba enter iscrtati naš poligon. Poligon će se
iscrtati različito ovisno o vrijednostima koordinata očišta i gledišta. Koordinate koje dobro prikazuju
objekt kocke su za očište npr. x = 2, y = 3, z = 4, a za gledište x = 0, y = 0, z = 0. Očište predstavlja
lokaciju od koje se gleda, a gledište lokaciju prema kojoj se gleda. Pošto su koordinate točaka kocke
blizu samog ishodišta sustava, ovaj pogled lijepo iscrtava taj objekt. Nakon što se objekt iscrtao,
omogućena je interakcija u smislu pomicanja koordinate očišta. Tipkama numpad-a 4 i 5 pomičemo x
koordinatu očišta, 7 i 8 y koordinatu očišta, a 8 i 2 z koordinate očišta. Ovom interakcijom moguće se
„gibati“ oko objekta te ga gledati iz različitih perspektiva.

### Komentar rezultata

Kao i kod prethodne vježbe, iscrtavanje objekta može trajati nekoliko sekundi ovisno o broju
poligona od kojih se objekt sastoji. Osim toga, nad svakom točkom se obavljaju operacije množenja
što još više produžuje njegovo iscrtavanje.. Isto kao kod prethodne vježbe, izbor objekta za
iscrtavanje se tvrdo zadaje u programskom kodu. Moguće promjene koje bi se mogle primijeniti su
uz pomicanja očišta, pomicanje gledišta. Time bi dobili bolji osjećaj slobodnog kretanja i gledanja u
trodimenzionalnom prostoru.


## VJEŽBA 6

### Opis programa

Ova vježba koristi sličnu implementaciju prethodnoj. Ravnine i točke objekata se iščitavaju iz istih
datoteka. Na isti način se izvodi transformacija točaka objekta ovisno o očištu i gledištu. Razlika je u
tome što se implementira računanje točaka Bezierove krivulje te se očište pomiče po njenim
točkama u zadanim razmacima od 0.01. Tim postupkom dobiva se dosta glatka animacija pomicanja.

### Korištene strukture podataka

Ova vježba koristi iste strukture podataka kao i prethodna vježba zbog slične implementacije. Točke
koje predstavljaju vrhove kontrolnog poligona spremljene su u polje s x, y i z vrijednostima. Za
spremanje točaka krivulje koristi se lista po kojoj će se očište kretati.

### Upute za korištenje programa

U ovoj vježbi nije potreban nikakav unos koordinata, već se neposredno nakon pokretanja, stvara
novi prozor i iscrtava rezultantna animacija. Nakon što animacija završi prozor se zatvara i program
završava.

### Komentar rezultata

Vrijeme izvođenja ovisi o korištenom objektu, tj. broju njegovih ravnina. Nedostatak programa je
interakcija. Naime, sve vrijednosti koje program koristi zadane su u programskom kodu i ne traže se
od korisnika. Promjena koja bi se mogla primijeniti u programu jest pomicanje gledišta, također
preko točaka još jedne Bezierove krivulje.


## VJEŽBA 7

### Opis programa

U ovoj vježbi, dodani su postupci sjenčanja tijela korištenih u prethodnim vježbama. Dodano je
uklanjanje poligona koje iz određene perspektive ne bi trebali biti vidljivi. Postupak uklanjanja
poligona sastoji se od određivanja normala poligona i vektora od očišta do središta poligona. Ako je
kut između izračunatih vektora između 0 i 90 stupnjeva, poligon nije stražnji, a ako je između 90 i
180 stupnjeva, poligon je stražnji i dodaje se u listu poligona koje je potrebno ukloniti. Nakon
uklanjanja poligona, definirana su: ambijentno svjetlo u cijelom prostoru i difuzno svjetlo s
odgovarajućim koordinatama izvora. Za svaku ravninu poligona određen je vektor normale te je na
temelju njegovog odnosa s vektorom iz izvora svjetlosti određen intenzitet difuznog svjetla tog
poligona. Također implementiran je Gouradov postupak sjenčanja koji se razlikuje od prethodnog u
tome što računa intenzitete osvjetljenja u svakom vrhu a ne svakom poligonu objekta. To se postiže
računanjem normala iz vrha, zbrajanjem svih normala ravnina koje sadržavaju taj vrh. Sjenčanje se
nakon toga određuje za svaku točku tijela interpolacijom boja triju vrhova pojedinog poligona. Svi
parametri u formuli za računanje intenziteta, tvrdo su zadani u kodu.

### Korištene strukture podataka

Pošto se ova vježba samo nadograđuje na implementacije prethodnih, koristit će iste strukture
podataka. Za intenzitete pojedinih točaka korištene su liste.

### Upute za korištenje programa

Ova vježba zahtjeva unose vrijednosti x, y, i z koordinata, redom za točke očišta, gledišta i izvora
svjetlosti. Nakon unosa, na ekranu se stvara crni prozor. Pristiskom gumba space iscrtava se žični
oblik objekta u kojem se može primijetiti uklanjanje stražnjih poligona. Gumbom enter, tijelu
dodajemo konstantno osvjetljenje koje se sastoji od ambijentnog i difuznog svjetla. Osvjetljenje tijela
je dosta oštro i nerealno zbog toga što smo za svaki vrh poligona zadali isti intenzitet. Ako želimo
ljepše osvjetljenje, pritiskom na tipku v prikazuje se osvjetljenje Gouraudovim postupkom. U svakom
trenutku moguće je tipkama space, enter i v promijeniti način sjenčanja. Isto tako, tipkama
numpada: 4 i 6, 7 i 9 i 8 i 2 moguće se pomicati kroz prostor promjenom koordinata točke očišta.

### Komentar rezultata

Vrijeme izvođenja ovisi o korištenom objektu, tj. broju njegovih ravnina. Moguća promjena koja bi se
mogla primijeniti jest interakcija s izvorom svjetlosti. To može uključivati pomicanje koordinata
samog izvora ili pak mijenjanje parametara njegovog osvjetljenja. Time bi postigli bolje
razumijevanje utjecaja udaljenosti izvora svjetlosti od objekta i odnosa u njihovim položajima
(kutevima).


## VJEŽBA 8

### Opis programa

Ova vježba sastoji se od dva radna zadatka. Prvi zadatak uključuje računanje Mandelbrotovog
fraktalnog skupa i ispisivanje na zaslon. Drugi zadatak računa Julijev fraktalni skup koji se razlikuje u
tome što se eksplicitno zadaje točka kompleksne ravnine c. Ako se točka c nalazi unutar
Mandelbrotovog skupa, Julijev skup će biti povezan, a inače nepovezan. Parametri skupa koji se
zadaju su maksimalan broj iteracija. Svi parametri i konstante zadani su u programskom kodu

### Korištene strukture podataka

Strukture podataka koje su korištene u ovoj vježbi su vrijednosti koordinata piksela na zaslonu,
spremljene u varijable u_min, u_max, v_min i v_max. Osim njih zadaju se maksimalan broj iteracija
m i vrijednost epsilon eps. Za kompleksne brojeve korištena je metoda complex koja prima
vrijednosti realnog i imaginarnog dijela, a omogućuje obavljanje operacija koje nisu moguće nad
samim realnim brojevima.

### Upute za korištenje programa

Programi oba zadatka se izvode odmah nakon pokretanja i nije potreban nikakav prethodni unos
podataka.

### Komentar rezultata

Nedostatak ovih programskih rješenja je jako dugo vrijeme izvođenja. Naime, nakon pokretanja
programa, ovisno o zadanim vrijednostima dimenzija prozora na kojem se fraktalni skup iscrtava,
trajanje može doseći više od 20 sekundi. Iz tog razloga vrijednosti dimenzija su postavljene na
300x300 kako bi što više skratili to izvođenje. Ono što bi se moglo primijeniti u izvođenju ovih
programa je interakcija zadavanja parametara fraktalnih skupova. Isto tako, moglo bi se dodati
paralelno iscrtavanje različito zadanih fraktalnih skupova na više prozora kako bi usporedili promjene
koje određeni parametri čine nad skupovima.


