import math
from pyglet.gl import *
eps = 100
m = 16
u_min, u_max, v_min, v_max = -1, 1, -1.2, 1.2
x_max, y_max = 300, 300
window = pyglet.window.Window(width=x_max, height=y_max)
c = complex(0.32, 0.043)

@window.event
def on_draw():
    for i in range(x_max):
        print(i)
        for j in range(y_max):
            #print(j)
            u0 = ((u_max - u_min) / x_max) * i + u_min
            v0 = ((v_max - v_min) / y_max) * j + v_min
            k = -1
            r = 0
            z = complex(u0, v0)
            while r < eps and k < m:
                k = k + 1
                z = z*z + c
                r = math.sqrt(math.pow(z.real, 2) + math.pow(z.imag, 2))
                #print(r)
                if r >= eps or k >= m:
                    break
            if k == m:
                pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (i, j)), ('c3f', (0, 0, 0)))
            else:
                pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (i, j)), ('c3f', (k/m, 1.0-k/m/2.0, 0.8-k/m/3.0)))


pyglet.app.run()
