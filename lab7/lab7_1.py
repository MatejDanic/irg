from pyglet.gl import *
import numpy as np
import math

f = open("lab4_objekti/domaci.obj.txt", "r")
tocke = []
ravnine = []
prvi = True

# citanje iz datoteke
for line in f:
    if line.startswith('v'):
        koordinate = line.split()
        tocka = [(float(koordinate[1])), (float(koordinate[2])), (float(koordinate[3]))]
        if prvi:
            x_max, x_min, y_max, y_min, z_max, z_min = tocka[0], tocka[0], tocka[1], tocka[1], tocka[2], tocka[2]
            prvi = False
        else:
            if tocka[0] > x_max:
                x_max = tocka[0]
            elif tocka[0] < x_min:
                x_min = tocka[0]
            if tocka[1] > y_max:
                y_max = tocka[1]
            elif tocka[1] < y_min:
                y_min = tocka[1]
            if tocka[2] > z_max:
                z_max = tocka[2]
            elif tocka[2] < z_min:
                z_min = tocka[2]
        tocke.append(tocka)

    elif line.startswith('f'):
        tocke_u_ravnini = line.split()
        ravnina = [int(tocke_u_ravnini[1])-1, int(tocke_u_ravnini[2])-1, int(tocke_u_ravnini[3])-1]
        ravnine.append(ravnina)

velicina_x = x_max - x_min
velicina_y = y_max - y_min
velicina_z = z_max - z_min
srediste_x = (x_max + x_min) / 2
srediste_y = (y_max + y_min) / 2
srediste_z = (z_max + z_min) / 2
max_raspon = 2 / max(velicina_x, velicina_y, velicina_z)
matrica_1 = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [-srediste_x, -srediste_y, -srediste_z, 1]]
matrica_2 = [[max_raspon, 0, 0, 0], [0, max_raspon, 0, 0], [0, 0, max_raspon, 0], [0, 0, 0, 1]]
m_uku = np.matmul(matrica_1, matrica_2)

tocke_2 = []
for tocka in tocke:
    tocka_2 = [(tocka[0] - srediste_x) * max_raspon,
               (tocka[1] - srediste_y) * max_raspon,
               (tocka[2] - srediste_z) * max_raspon]
    tocke_2.append(tocka_2)
tocke = tocke_2
print("--------------")

# print(tocke)
# print(ravnine)
print("Ociste: ")
o = [10.6, 4.7, 0.3]
print("Glediste: ")
g = [0, 0, 0]
print("Izvor svjetla: ")
izvor = [5, 5, 5]
print(o, g, izvor)

def izbrisi_straznje_poligone():
    straznji_poligoni = []
    for ravnina in ravnine:
        tocka_1, tocka_2, tocka_3 = tocke[ravnina[0]], tocke[ravnina[1]], tocke[ravnina[2]]

        a = (tocka_2[1] - tocka_1[1]) * (tocka_3[2] - tocka_1[2]) - (tocka_2[2] - tocka_1[2]) * (
                    tocka_3[1] - tocka_1[1])
        b = (tocka_2[2] - tocka_1[2]) * (tocka_3[0] - tocka_1[0]) - (tocka_2[0] - tocka_1[0]) * (
                    tocka_3[2] - tocka_1[2])
        c = (tocka_2[0] - tocka_1[0]) * (tocka_3[1] - tocka_1[1]) - (tocka_2[1] - tocka_1[1]) * (
                    tocka_3[0] - tocka_1[0])
        d = -tocka_1[0] * a - tocka_1[1] * b - tocka_1[2] * c

        sred_x = (tocka_1[0] + tocka_2[0] + tocka_3[0]) / 3
        sred_y = (tocka_1[1] + tocka_2[1] + tocka_3[1]) / 3
        sred_z = (tocka_1[2] + tocka_2[2] + tocka_3[2]) / 3

        normala_i = o[0] - sred_x
        normala_j = o[1] - sred_y
        normala_k = o[2] - sred_z

        cosa = (a * normala_i + b * normala_j + c * normala_k) / (math.sqrt(
            math.pow(a, 2) + math.pow(b, 2) + math.pow(c, 2)) \
            * math.sqrt(math.pow(normala_i, 2) + math.pow(normala_j, 2) + math.pow(normala_k, 2)))
        #print(cosa)
        kut = math.degrees(math.acos(cosa))
        #print(kut)
        if kut > 90:
            straznji_poligoni.append(ravnina)
    return straznji_poligoni


def izracunaj_osvjetljenje(straznji_poligoni, izvor):
    ia = 250
    ka = 0.2
    ii = 250
    kd = 0.8
    intenziteti = []
    for rav in ravnine:
        intenzitet = ia * ka
        if rav not in straznji_poligoni:
            tocka_1, tocka_2, tocka_3 = tocke[rav[0]], tocke[rav[1]], tocke[rav[2]]

            normala_i = (tocka_2[1] - tocka_1[1]) * (tocka_3[2] - tocka_1[2]) - \
                        (tocka_2[2] - tocka_1[2]) * (tocka_3[1] - tocka_1[1])
            normala_j = (tocka_2[2] - tocka_1[2]) * (tocka_3[0] - tocka_1[0]) - \
                        (tocka_2[0] - tocka_1[0]) * (tocka_3[2] - tocka_1[2])
            normala_k = (tocka_2[0] - tocka_1[0]) * (tocka_3[1] - tocka_1[1]) - \
                        (tocka_2[1] - tocka_1[1]) * (tocka_3[0] - tocka_1[0])
            normala_dulj = math.sqrt(math.pow(normala_i, 2) + math.pow(normala_j, 2) + math.pow(normala_k, 2))
            #print(normala_i, normala_j, normala_k)
            sred_x = (tocka_1[0] + tocka_2[0] + tocka_3[0]) / 3
            sred_y = (tocka_1[1] + tocka_2[1] + tocka_3[1]) / 3
            sred_z = (tocka_1[2] + tocka_2[2] + tocka_3[2]) / 3

            l_i = izvor[0] - sred_x
            l_j = izvor[1] - sred_y
            l_k = izvor[2] - sred_z
            l_dulj = math.sqrt(math.pow(l_i, 2) + math.pow(l_j, 2) + math.pow(l_k, 2))

            normala_i /= normala_dulj
            normala_j /= normala_dulj
            normala_k /= normala_dulj
            l_i /= l_dulj
            l_j /= l_dulj
            l_k /= l_dulj

            ln = normala_i * l_i + normala_j * l_j + normala_k * l_k
            if ln >= 0:
                #print(intenzitet)
                intenzitet += ii * kd * ln
                #print(intenzitet)
            intenziteti.append([intenzitet, intenzitet, intenzitet])
    #print(intenziteti)
    return intenziteti


def izracunaj_gouraud(straznji_poligoni, izvor):
    ia = 250
    ka = 0.2
    ii = 250
    kd = 0.8
    index_tocke = 0
    normale = {}
    print(len(tocke))

    for tocka in tocke:
        print("index: ", index_tocke)
        n_i, n_j, n_k = 0, 0, 0
        for rav in ravnine:
            #print(rav)
            if rav not in straznji_poligoni:
                #print("Ravnina ", rav, "ne nalazi se u straznjim poligonima ", straznji_poligoni)
                #print(index_tocke, " postoji  u ", rav, index_tocke == rav[0] or index_tocke == rav[1] or index_tocke == rav[2])
                if index_tocke == rav[0] or index_tocke == rav[1] or index_tocke == rav[2]:
                    print("tocka ", index_tocke, " nalazi se u ravnini ", rav)
                    tocka_1, tocka_2, tocka_3 = tocke[rav[0]], tocke[rav[1]], tocke[rav[2]]
                    normala_i = (tocka_2[1] - tocka_1[1]) * (tocka_3[2] - tocka_1[2]) - \
                                (tocka_2[2] - tocka_1[2]) * (tocka_3[1] - tocka_1[1])
                    normala_j = (tocka_2[2] - tocka_1[2]) * (tocka_3[0] - tocka_1[0]) - \
                                (tocka_2[0] - tocka_1[0]) * (tocka_3[2] - tocka_1[2])
                    normala_k = (tocka_2[0] - tocka_1[0]) * (tocka_3[1] - tocka_1[1]) - \
                                (tocka_2[1] - tocka_1[1]) * (tocka_3[0] - tocka_1[0])
                    normala_dulj = math.sqrt(math.pow(normala_i, 2) + math.pow(normala_j, 2) + math.pow(normala_k, 2))
                    normala_i /= normala_dulj
                    normala_j /= normala_dulj
                    normala_k /= normala_dulj
                    n_i += normala_i
                    n_j += normala_j
                    n_k += normala_k
        n_i /= 4
        n_j /= 4
        n_k /= 4
        normale[index_tocke] = [n_i, n_j, n_k]
        index_tocke += 1

    intenziteti = []
    for rav in ravnine:
        if rav not in straznji_poligoni:
            tocka_1, tocka_2, tocka_3 = tocke[rav[0]], tocke[rav[1]], tocke[rav[2]]

            # 1. tocka
            intenzitet_1 = ia * ka
            l_i = izvor[0] - tocka_1[0]
            l_j = izvor[1] - tocka_1[1]
            l_k = izvor[2] - tocka_1[2]
            l_dulj = math.sqrt(math.pow(l_i, 2) + math.pow(l_j, 2) + math.pow(l_k, 2))
            l_i /= l_dulj
            l_j /= l_dulj
            l_k /= l_dulj
            n_i, n_j, n_k = normale[rav[0]]
            ln = n_i * l_i + n_j * l_j + n_k * l_k
            if ln >= 0:
                # print(intenzite
                intenzitet_1 += ii * kd * ln

            # 2. tocka
            intenzitet_2 = ia * ka
            l_i = izvor[0] - tocka_2[0]
            l_j = izvor[1] - tocka_2[1]
            l_k = izvor[2] - tocka_2[2]
            l_dulj = math.sqrt(math.pow(l_i, 2) + math.pow(l_j, 2) + math.pow(l_k, 2))
            l_i /= l_dulj
            l_j /= l_dulj
            l_k /= l_dulj
            n_i, n_j, n_k = normale[rav[1]]
            ln = n_i * l_i + n_j * l_j + n_k * l_k
            if ln >= 0:
                # print(intenzite
                intenzitet_2 += ii * kd * ln

            # 3. tocka
            intenzitet_3 = ia * ka
            l_i = izvor[0] - tocka_3[0]
            l_j = izvor[1] - tocka_3[1]
            l_k = izvor[2] - tocka_3[2]
            l_dulj = math.sqrt(math.pow(l_i, 2) + math.pow(l_j, 2) + math.pow(l_k, 2))
            l_i /= l_dulj
            l_j /= l_dulj
            l_k /= l_dulj
            n_i, n_j, n_k = normale[rav[2]]
            ln = n_i * l_i + n_j * l_j + n_k * l_k
            if ln >= 0:
                # print(intenzite
                intenzitet_3 += ii * kd * ln
            intenziteti.append([intenzitet_1, intenzitet_2, intenzitet_3])
    #print(intenziteti)
    return intenziteti


def izracunaj_tocke(ociste, glediste) :
    matrica_t = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [-ociste[0], -ociste[1], -ociste[2], 1]]
    glediste1 = np.matmul([glediste[0], glediste[1], glediste[2], 1], matrica_t)

    sina = glediste1[1] / (np.sqrt(np.square(glediste1[0]) + np.square(glediste1[1])))
    cosa = glediste1[0] / (np.sqrt(np.square(glediste1[0]) + np.square(glediste1[1])))
    matrica_t2 = [[cosa, -sina, 0, 0], [sina, cosa, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
    glediste2 = np.matmul(glediste1, matrica_t2)

    sinb = glediste2[0] / (np.sqrt(np.square(glediste2[0]) + np.square(glediste2[2])))
    cosb = glediste2[2] / (np.sqrt(np.square(glediste2[0]) + np.square(glediste2[2])))
    matrica_t3 = [[cosb, 0, sinb, 0], [0, 1, 0, 0], [-sinb, 0, cosb, 0], [0, 0, 0, 1]]

    matrica_t4 = [[0, -1, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
    matrica_t5 = [[-1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    matrica_t12 = np.matmul(matrica_t, matrica_t2)
    matrica_t123 = np.matmul(matrica_t12, matrica_t3)
    matrica_t1234 = np.matmul(matrica_t123, matrica_t4)
    matrica_t_uk = np.matmul(matrica_t1234, matrica_t5)

    udaljenost_h = np.sqrt(np.square(ociste[0] - glediste[0])
                           + np.square(ociste[1] - glediste[1])
                           + np.square(ociste[2] - glediste[2]))
    matrica_p = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1 / udaljenost_h], [0, 0, 0, 0]]
    nove_tocke = []

    # transformacija tocaka
    for i in range(len(tocke)):
        nova_tocka = np.matmul([tocke[i][0], tocke[i][1], tocke[i][2], 1], matrica_t_uk)
        nova_tocka = np.matmul(nova_tocka, matrica_p)
        nova_tocka = np.multiply(nova_tocka, 1 / nova_tocka[3])
        nove_tocke.append(nova_tocka)
    return nove_tocke


nove_tocke = izracunaj_tocke(o, g)
window = pyglet.window.Window(width=800, height=800)
intenziteti = []
kliknut = 1


@window.event
def on_key_press(button, modifier):
    global intenziteti
    global kliknut
    print(button)
    straznji_poligoni = izbrisi_straznje_poligone()
    if button == 65361:
        o[1] -= 1
    elif button == 65362:
        o[2] += 1
    elif button == 65363:
        o[1] += 1
    elif button == 65364:
        o[2] -= 1
    elif button == 65360:
        o[0] += 1
    elif button == 65365:
        o[0] -= 1
    elif button == 32:
        kliknut = 1
    elif button == 65293:
        kliknut = 2
    elif button == 118:
        kliknut = 3
    if kliknut == 2:
        intenziteti = izracunaj_osvjetljenje(straznji_poligoni, izvor)
    elif kliknut == 3:
        intenziteti = izracunaj_gouraud(straznji_poligoni, izvor)
    else:
        intenziteti = []
    print("Ociste: ", o)
    window.clear()
    scale = 100
    translate = 200
    azurirane_tocke = izracunaj_tocke(o, g)
    brojac = 0
    print("straznjiii: ", straznji_poligoni)
    for rav in ravnine:
        if True:
            print(rav)
            x1 = azurirane_tocke[rav[0]][0]
            y1 = azurirane_tocke[rav[0]][1]
            z1 = azurirane_tocke[rav[0]][2]
            x2 = azurirane_tocke[rav[1]][0]
            y2 = azurirane_tocke[rav[1]][1]
            z2 = azurirane_tocke[rav[1]][2]
            x3 = azurirane_tocke[rav[2]][0]
            y3 = azurirane_tocke[rav[2]][1]
            z3 = azurirane_tocke[rav[2]][2]
            # print(x1, y1, x2, y2, x3, y3)
            if len(intenziteti) > 0:
                #print(intenziteti[brojac])
                pyglet.graphics.draw(3, pyglet.gl.GL_TRIANGLES,
                                     ('v2f', (int(x1 * scale + translate), int(y1 * scale + translate),
                                              int(x2 * scale + translate), int(y2 * scale + translate),
                                              int(x3 * scale + translate), int(y3 * scale + translate))),
                                     ('c3f', (0, intenziteti[brojac][0]/255, 0,
                                              0, intenziteti[brojac][1]/255, 0,
                                              0, intenziteti[brojac][2]/255, 0)))
                brojac += 1
                if brojac >= len(intenziteti):
                    brojac = 0
            else:
                pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                                     ('v2i', (int(x1 * scale + translate), int(y1 * scale + translate),
                                              int(x2 * scale + translate), int(y2 * scale + translate))))
                pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                                     ('v2i', (int(x1 * scale + translate), int(y1 * scale + translate),
                                              int(x3 * scale + translate), int(y3 * scale + translate))))
                pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                                     ('v2i', (int(x2 * scale + translate), int(y2 * scale + translate),
                                              int(x3 * scale + translate), int(y3 * scale + translate))))

pyglet.app.run()



