A = [8.9, 8.5, -8.5]
B = [-1.5, 7.5, 3.8]
C = [-8.4, 3.3, 5.5]
D = [-4.9, 7.1, 7.3]
O = [-5.7, 0.0, -14.8]

def poly(a,b,c,p,o):
    d = b[0]-a[0]
    e = b[1]-a[1]
    f = b[2]-a[2]
    g = c[0]-a[0]
    h = c[1]-a[1]
    i = c[2]-a[2]
    A1 = e*i - f*h
    B1 = f*g - d*i
    C1 = d*h - e*g
    D1 = - (A1 * a[0] + B1 * a[1] + C1 * a[2])
    var1 = A1 * p[0] + B1 * p[1] + C1 * p[2] + D1
    var2 = A1 * o[0] + B1 * o[1] + C1 * o[2] + D1
    if (var1 > 0 and var2 > 0):
        return 0
    elif (var1 < 0 and var2 < 0):
        return 0
    return 1

P1 = poly(A,B,C,D,O)
P2 = poly(B,C,D,A,O)
P3 = poly(C,D,A,B,O)
P4 = poly(D,A,B,C,O)
rjesenje = ""
if (P1==1): rjesenje += "P1 "
if (P2==1): rjesenje += "P2 "
if (P3==1): rjesenje += "P3 "
if (P4==1): rjesenje += "P4 "
print(rjesenje)