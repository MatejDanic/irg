from pyglet.gl import *

f = open("lab4_objekti/domaci2.obj.txt", "r")
tocke = []
ravnine = []


class Tocka:
    def __init__(self, x_coor, y_coor, z_coor):
        self.x_coor = x_coor
        self.y_coor = y_coor
        self.z_coor = z_coor

    def __repr__(self):
        return '({}, {}, {})'.format(self.x_coor, self.y_coor, self.z_coor)


class Ravnina:
    def __init__(self, t1, t2, t3):
        self.t1 = t1
        self.t2 = t2
        self.t3 = t3

    def __repr__(self):
        return '({}, {}, {})'.format(self.t1, self.t2, self.t3)


for line in f:
    if line.startswith('v'):
        koordinate = line.split()
        tocke.append(Tocka(int(float(koordinate[1])), int(float(koordinate[2])), int(float(koordinate[3]))))
    elif line.startswith('f'):
        tocke_u_ravnini = line.split()
        ravnine.append(Ravnina(int(tocke_u_ravnini[1])-1, int(tocke_u_ravnini[2])-1, int(tocke_u_ravnini[3])-1))
print(tocke)
print(ravnine)
v = Tocka(float(input()), float(input()), float(input()))
print(v)
window = pyglet.window.Window(width=800, height=800)


@window.event
def on_draw():
    glClearColor(0, 0, 0, 0)


@window.event
def on_key_press(button, modifier):
    if button == 32:
        scale = 0.1
        translate = 250
        for i in range(len(ravnine)):
            x1 = tocke[ravnine[i].t1].x_coor
            y1 = tocke[ravnine[i].t1].y_coor
            z1 = tocke[ravnine[i].t1].z_coor
            x2 = tocke[ravnine[i].t2].x_coor
            y2 = tocke[ravnine[i].t2].y_coor
            z2 = tocke[ravnine[i].t2].z_coor
            x3 = tocke[ravnine[i].t3].x_coor
            y3 = tocke[ravnine[i].t3].y_coor
            z3 = tocke[ravnine[i].t3].z_coor

            pyglet.graphics.draw(3, pyglet.gl.GL_LINES, ('v2i', (int(x1*scale+translate), int(y1*scale+translate),
                                                                 int(x2*scale+translate), int(y2*scale+translate),
                                                                 int(x3*scale+translate), int(y3*scale+translate))))

        pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (int(v.x_coor*scale+translate),
                                                              int(v.y_coor*scale+translate))))

        tocka_u_tijelu(v)


def tocka_u_tijelu(v):
    unutar = True
    for i in range(len(ravnine)):
        x1 = tocke[ravnine[i].t1].x_coor
        y1 = tocke[ravnine[i].t1].y_coor
        z1 = tocke[ravnine[i].t1].z_coor
        x2 = tocke[ravnine[i].t2].x_coor
        y2 = tocke[ravnine[i].t2].y_coor
        z2 = tocke[ravnine[i].t2].z_coor
        x3 = tocke[ravnine[i].t3].x_coor
        y3 = tocke[ravnine[i].t3].y_coor
        z3 = tocke[ravnine[i].t3].z_coor

        a = (y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1)
        b = -(x2 - x1) * (z3 - z1) + (z2 - z1) * (x3 - x1)
        c = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)
        d = -x1 * a - y1 * b - z1 * c

        print('{} < 0 is {}'.format(a * v.x_coor + b * v.y_coor + c * v.z_coor + d,
                                    a * v.x_coor + b * v.y_coor + c * v.z_coor + d < 0))
        if a * v.x_coor + b * v.y_coor + c * v.z_coor + d >= 0:
            unutar = False
            break

    print('tocka ({}, {}, {}) je {} poligona'.format(v.x_coor, v.y_coor, v.z_coor, 'unutar' if unutar else 'izvan'))


pyglet.app.run()
