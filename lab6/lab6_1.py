from pyglet.gl import *
import numpy as np
import math
import time

f = open("lab4_objekti/kocka.obj.txt", "r")
tocke = []
ravnine = []

# citanje iz datoteke
for line in f:
    if line.startswith('v'):
        koordinate = line.split()
        tocka = [(float(koordinate[1])), (float(koordinate[2])), (float(koordinate[3]))]
        tocke.append(tocka)
    elif line.startswith('f'):
        tocke_u_ravnini = line.split()
        ravnina = [int(tocke_u_ravnini[1])-1, int(tocke_u_ravnini[2])-1, int(tocke_u_ravnini[3])-1]
        ravnine.append(ravnina)

print(tocke)
print(ravnine)
print("Ociste: ")
o = [10, 10, 10]
print("Glediste: ")
g = [0, 0, 0]
print(o, g)
r0 = [10, 10, 10]
r1 = [-10, 10, -10]
r2 = [-20, -10, 0]
krivulja = [r0, r1, r2]
tocke_krivulje = []
t = 0.0

# racunanje tocaka krivulje
while t <= 1.0:
    tocka_krivulje = [0, 0, 0]
    for k in range(len(krivulja)):
        b = ((math.factorial(len(krivulja)) / (math.factorial(k))) * math.factorial(len(krivulja) - k)) \
            * math.pow(t, k) * math.pow(1 - t, len(krivulja) - k)
        tocka_krivulje += np.multiply(krivulja[k], b)
        print(tocka_krivulje)
    tocke_krivulje.append(tocka_krivulje)
    t += 0.01
print(len(tocke_krivulje))


def izracunaj_tocke(ociste, glediste):
    matrica_t = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [-ociste[0], -ociste[1], -ociste[2], 1]]
    glediste1 = np.matmul([glediste[0], glediste[1], glediste[2], 1], matrica_t)

    sina = glediste1[1] / (np.sqrt(np.square(glediste1[0]) + np.square(glediste1[1])))
    cosa = glediste1[0] / (np.sqrt(np.square(glediste1[0]) + np.square(glediste1[1])))
    matrica_t2 = [[cosa, -sina, 0, 0], [sina, cosa, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
    glediste2 = np.matmul(glediste1, matrica_t2)

    sinb = glediste2[0] / (np.sqrt(np.square(glediste2[0]) + np.square(glediste2[2])))
    cosb = glediste2[2] / (np.sqrt(np.square(glediste2[0]) + np.square(glediste2[2])))
    matrica_t3 = [[cosb, 0, sinb, 0], [0, 1, 0, 0], [-sinb, 0, cosb, 0], [0, 0, 0, 1]]

    matrica_t4 = [[0, -1, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
    matrica_t5 = [[-1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    matrica_t12 = np.matmul(matrica_t, matrica_t2)
    matrica_t123 = np.matmul(matrica_t12, matrica_t3)
    matrica_t1234 = np.matmul(matrica_t123, matrica_t4)
    matrica_t_uk = np.matmul(matrica_t1234, matrica_t5)

    udaljenost_h = np.sqrt(np.square(ociste[0] - glediste[0])
                           + np.square(ociste[1] - glediste[1])
                           + np.square(ociste[2] - glediste[2]))
    matrica_p = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1 / udaljenost_h], [0, 0, 0, 0]]
    nove_tocke = []

    # transformacija tocaka
    for i in range(len(tocke)):
        nova_tocka = np.matmul([tocke[i][0], tocke[i][1], tocke[i][2], 1], matrica_t_uk)
        nova_tocka = np.matmul(nova_tocka, matrica_p)
        nova_tocka = np.multiply(nova_tocka, 1 / nova_tocka[3])
        nove_tocke.append(nova_tocka)
    return nove_tocke


window = pyglet.window.Window(width=800, height=800)
broj = 0


@window.event
def on_draw():
    global broj
    window.clear()
    scale = 100
    translate = 200
    if broj >= 90:
        broj = 90
    oc = tocke_krivulje[broj]
    azurirane_tocke = izracunaj_tocke(oc, g)
    #print(oc)
    broj += 1
    for rav in ravnine:
        x1 = azurirane_tocke[rav[0]][0]
        y1 = azurirane_tocke[rav[0]][1]
        z1 = azurirane_tocke[rav[0]][2]
        x2 = azurirane_tocke[rav[1]][0]
        y2 = azurirane_tocke[rav[1]][1]
        z2 = azurirane_tocke[rav[1]][2]
        x3 = azurirane_tocke[rav[2]][0]
        y3 = azurirane_tocke[rav[2]][1]
        z3 = azurirane_tocke[rav[2]][2]
        # print(x1, y1, x2, y2, x3, y3)
        pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                             ('v2i', (int(x1 * scale + translate), int(y1 * scale + translate),
                                      int(x2 * scale + translate), int(y2 * scale + translate))))
        pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                             ('v2i', (int(x1 * scale + translate), int(y1 * scale + translate),
                                      int(x3 * scale + translate), int(y3 * scale + translate))))
        pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                             ('v2i', (int(x2 * scale + translate), int(y2 * scale + translate),
                                      int(x3 * scale + translate), int(y3 * scale + translate))))


for broj in range(len(tocke_krivulje)):
    time.sleep(0.01)
    pyglet.clock.tick()
    window.switch_to()
    window.dispatch_events()
    window.dispatch_event('on_draw')
    window.flip()
pyglet.app.run()

