from pyglet.gl import *

window = pyglet.window.Window(width=400, height=400, resizable='false')
tocke = []
poligon_nacrtan = False
a, b, c = [], [], []


class Tocka:
    def __init__(self, x_coor, y_coor):
        self.x_coor = x_coor
        self.y_coor = y_coor

    def __repr__(self):
        return '({}, {})'.format(self.x_coor, self.y_coor)


@window.event
def on_mouse_press(x, y, button, modifiers):
    global poligon_nacrtan
    if not poligon_nacrtan:
        tocke.append(Tocka(x, y))
    else:
        v = Tocka(x, y)
        pyglet.graphics.draw(1, pyglet.gl.GL_POINTS, ('v2i', (x, y)))
        dot_in_polygon(v)


@window.event
def on_key_press(button, modifier):
    if button == 65293:
        print(tocke)
        draw_polygon()
    elif button == 32:
        color_polygon()


def color_polygon():
    x_min = tocke[0].x_coor
    y_min = tocke[0].y_coor
    x_max = tocke[0].x_coor
    y_max = tocke[0].y_coor

    for i in range(len(tocke)):
        if tocke[i].x_coor > x_max:
            x_max = tocke[i].x_coor
        elif tocke[i].x_coor < x_min:
            x_min = tocke[i].x_coor
        if tocke[i].y_coor > y_max:
            y_max = tocke[i].y_coor
        elif tocke[i].y_coor < y_min:
            y_min = tocke[i].y_coor

    for j in range(y_min, y_max):
        l = x_min
        d = x_max
        for i in range(len(tocke)):
            if a[i] != 0:
                x1 = (-b[i] * j - c[i]) / a[i]
                if i < len(tocke)-1:
                    if tocke[i].y_coor < tocke[i+1].y_coor:
                        if x1 > l:
                            l = x1
                    else:
                        if x1 < d:
                            d = x1
                else:
                    if tocke[i].y_coor < tocke[0].y_coor:
                        if x1 > l:
                            l = x1
                    else:
                        if x1 < d:
                            d = x1
        if l < d:
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ('v2i', (int(l), j, int(d), j)))


def dot_in_polygon(T):
    unutar = True
    for i in range(len(tocke)):
        print('{} * {} + {} * {} + {} > 0 is {}'.format(T.x_coor, a[i], T.y_coor, b[i], c[i],
                                                        T.x_coor * a[i] + T.y_coor * b[i] + c[i] > 0))
        if T.x_coor * a[i] + T.y_coor * b[i] + c[i] > 0:
            print('tocka ({}, {}) je izvan poligona'.format(T.x_coor, T.y_coor))
            unutar = False
            break
    if unutar:
        print('tocka ({}, {}) je unutar poligona'.format(T.x_coor, T.y_coor))


def draw_polygon():
    global poligon_nacrtan,  a, b, c
    for i in range(1, len(tocke)):
        x1, y1 = tocke[i-1].x_coor, tocke[i-1].y_coor
        x2, y2 = tocke[i].x_coor, tocke[i].y_coor

        pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ('v2i', (x1, y1, x2, y2)))
        if i == len(tocke)-1:
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES, ('v2i', (x2, y2, tocke[0].x_coor, tocke[0].y_coor)))
    poligon_nacrtan = True
    for i in range(len(tocke)):
        if i < (len(tocke)-1):
            a.append(tocke[i].y_coor - tocke[i+1].y_coor)
            b.append(-tocke[i].x_coor + tocke[i+1].x_coor)
            c.append(tocke[i].x_coor * tocke[i+1].y_coor - tocke[i+1].x_coor * tocke[i].y_coor)
        else:
            print(i)
            a.append(tocke[i].y_coor - tocke[0].y_coor)
            b.append(-tocke[i].x_coor + tocke[0].x_coor)
            c.append(tocke[i].x_coor * tocke[0].y_coor - tocke[0].x_coor * tocke[i].y_coor)


@window.event
def on_draw():
    glClearColor(0, 0, 0, 0)


pyglet.app.run()
